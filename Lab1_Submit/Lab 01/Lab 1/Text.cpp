///--------------------------------------------------------------------
///
///  Laboratory 1                                           test1.cpp
///
///  Solution: Array implementation of the ADT
///
///--------------------------------------------------------------------

///Lab 01
#ifdef _MSC_VER
#pragma warning(push)
#pragma warning(disable : 4996)
#endif



#include <iostream>
#include <iomanip>
#include <cassert>
#include <cstring>
#include <string.h>
#include "Text.h"
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
Text::Text(const char *charSeq)
{
	///Creates a string containing the delimited sequence of characters
	///charSeq. Allocates enough memory for this string.

	///Account for null
	try
	{
		string newString = charSeq;
		bufferSize = newString.length();
		buffer = new char[bufferSize];
	}
	catch (bad_alloc &e)
	{
		cout << "Exception caught." << endl;
	}
	strcpy(buffer, charSeq);
	buffer = buffer + '\0';
}
/*void Text::printBuffer()
{
	int i = 0;
	cout << bufferSize << endl;
	while (buffer[i] != NULL)
	{
		cout << buffer[i];
		i++;
	}
}
*/
Text::Text(const Text &valueText)
{
	///Copy constructor, creates a copy of valueText. Called whenever
	///A string is passed to a function using call by value, a function returns a string, or a string is initialized using another string
	///as is in the declarations

	buffer = valueText.buffer;
	bufferSize = valueText.bufferSize;
}
Text::~Text()
{
	//delete[] buffer;
}
void Text::operator = (const Text& other)
{
	bufferSize = other.bufferSize;
	buffer = other.buffer;

}

int Text::getLength() const
{
	return strlen(buffer);
}
char Text::operator [] (int n) const
{
	///Returns the nth character in a Text object where the characters are numbered beginning with zero
	int place = n;
	return this->buffer[n];
}

istream & operator >> (istream &input, Text &inputText)

/// Text input function. Extracts a string from istream input and
/// returns it in inputText. Returns the state of the input stream.

{
	const int textBufferSize = 256;     /// Large (but finite)
	char textBuffer[textBufferSize];   /// text buffer

	/// Read a string into textBuffer, setw is used to prevent buffer
	/// overflow.

	input >> setw(textBufferSize) >> textBuffer;

	/// Apply the Text(char*) constructor to convert textBuffer to
	/// a string. Assign the resulting string to inputText using the
	/// assignment operator.

	inputText = textBuffer;

	/// Return the state of the input stream.

	return input;
}

//--------------------------------------------------------------------
void Text::clear()

/// Clears a text object ie makes it empty. 
/// The buffer size remains unchanged.

{
	buffer = nullptr;
	bufferSize = 0;

}

void Text::showStructure() const

/// Outputs the characters in a string. 
///	This Operation is intended for testing/debugging purposes only.

{

	if (buffer != nullptr)
	{
		cout << buffer << endl;
	}
	
}

ostream & operator << (ostream &output, const Text &outputText)

/// Text output function. Inserts outputText in ostream output.
/// Returns the state of the output stream.

{
	output << outputText.buffer;
	return output;
}
#ifdef _MSC_VER
#pragma warning(pop)
#endif